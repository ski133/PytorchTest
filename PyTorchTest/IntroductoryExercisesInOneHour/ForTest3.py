import torch as t
from matplotlib import pyplot as plt
from IPython import display
import numpy


#设置随机种子
t.manual_seed(1000)

#定义函数关系
def get_fake_date(batch_size=8):
    x=t.rand(batch_size,1)*20
    y=x*2+(1+t.randn(batch_size,1))*3
    return x,y
#x,y=get_fake_date()
#plt.scatter(x.squeeze().numpy(),y.squeeze().numpy())
#plt.show()

#随机初始化参数
w=t.rand(1,1)
b=t.zeros(1,1)
lr=0.001

for ii in range(20000):
    x,y=get_fake_date()
    #forword:
    y_pred=x.mm(w)+b.expand_as(y)
    loss=0.5*(y_pred-y)**2
    loss=loss.sum()

    #backword:
    dloss=1
    dy_pred=dloss*(y_pred-y)
    dw=x.t().mm(dy_pred)
    db=dy_pred.sum()

    w.sub_(lr*dw)
    b.sub_(lr*db)
    if ii%1000 ==0:
        display.clear_output(wait=True)
        x=t.arange(0,20).view(-1,1)
        y=x.mm(w)+b.expand_as(x)
        plt.plot(x.numpy(),y.numpy())
        x2,y2=get_fake_date(batch_size=20)
        plt.scatter(x2.numpy(),y2.numpy())
        plt.xlim(0,20)
        plt.ylim(0,41)
        plt.show()
        plt.pause(0.5)
print(w.squeeze()[0],b.squeeze()[0])