import torch
import torch.nn as nn
import torch.nn.functional as f
from torch.autograd import Variable
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.con1=nn.Conv2d(1, 6, 5)
        #print(self.con1)
        #self.pool=nn.MaxPool2d(2,2)
        self.con2=nn.Conv2d(6,16,5)
        self.fc1=nn.Linear(16*5*5,120)
        self.fc2=nn.Linear(120,84)
        self.fc3=nn.Linear(84,10)
    def forward(self,x):
        x=f.max_pool2d(f.relu(self.con1(x)),(2,2))
        x=f.max_pool2d(f.relu(self.con2(x)),2)
        x=x.view(-1,16*5*5)
        x=f.relu(self.fc1(x))
        x=f.relu(self.fc2(x))
        x=self.fc3(x)
        return x
net=Net()
#print(net)
params = list(net.parameters())
#print(params)
#for name , parameters in net.named_parameters():
    #print(name,':',parameters.size())
input = Variable(torch.randn(1, 1, 32, 32))
out = net(input)
print(out)
net.zero_grad()
#print("before:",net.con1.bias.grad)
#print(out.backward(torch.ones(1,10)))
output = net(input)
target =Variable(torch.arange(0,10))
criterion = nn.MSELoss()
loss = criterion(output,target)
print(loss.backward())
loss