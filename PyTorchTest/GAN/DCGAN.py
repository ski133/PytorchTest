# coding:utf-8
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
from torchvision.utils import save_image
import os

gpu_id=None

if gpu_id is not None:
    os.environ['CUDA_VISIBLE_DEVICES']=gpu_id
    device=torch.device('cuda')
else:device = torch.device('cpu')
if os.path.exists('DCgan_images') is False:
    os.mkdir('DCgan_images')
z_dim = 100
batch_size = 64
learning_rate = 0.0002
total_epochs = 200


def weights_init_normal(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm2d') != -1:
        torch.nn.init.normal_(m.weight.data, 1.0, 0.02)
        torch.nn.init.constant_(m.bias.data, 0.0)


class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()
        conv = []
        conv.append(nn.Conv2d(in_channels=1, out_channels=16, kernel_size=3, stride=2, padding=1))
        conv.append(nn.LeakyReLU(0.2, inplace=True))

        conv.append(nn.Conv2d(in_channels=16, out_channels=32, kernel_size=3, stride=2, padding=1))
        conv.append(nn.BatchNorm2d(32))
        conv.append(nn.LeakyReLU(0.2, inplace=True))

        conv.append(nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, stride=2, padding=1))
        conv.append(nn.BatchNorm2d(64))
        conv.append(nn.LeakyReLU(0.2, inplace=True))

        conv.append(nn.Conv2d(in_channels=64, out_channels=128, kernel_size=4, stride=1))
        conv.append(nn.BatchNorm2d(128))
        conv.append(nn.LeakyReLU(0.2, inplace=True))

        self.conv = nn.Sequential(*conv)
        self.linear = nn.Sequential(nn.Linear(in_features=128, out_features=1), nn.Sigmoid())

    def forward(self, x):
        x = self.conv(x)
        x = x.view(x.size(0), -1)
        validity = self.linear(x)
        return validity


class Generator(nn.Module):
    def __init__(self, z_dim):
        super(Generator, self).__init__()

        self.z_dim = z_dim
        layers = []
        self.linear = nn.Linear(self.z_dim, 4 * 4 * 256)

        layers.append(nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=3, stride=2, padding=0))
        layers.append(nn.BatchNorm2d(128))
        layers.append(nn.ReLU(inplace=True))

        layers.append(nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=3, stride=2, padding=1))
        layers.append(nn.BatchNorm2d(64))
        layers.append(nn.ReLU(inplace=True))

        layers.append(nn.ConvTranspose2d(in_channels=64, out_channels=1, kernel_size=4, stride=2, padding=2))
        layers.append(nn.Tanh())

        self.model = nn.Sequential(*layers)

    def forward(self, z):
        x = self.linear(z)
        x = x.view([x.size(0), 256, 4, 4])
        x = self.model(x)
        return x


dnet = Discriminator().to(device)
gnet = Generator(z_dim=z_dim).to(device)
gnet.apply(weights_init_normal)
dnet.apply(weights_init_normal)

bce = nn.BCELoss().to(device)
ones = torch.ones(batch_size).to(device)
zeros = torch.zeros(batch_size).to(device)

g_optimizer = optim.Adam(gnet.parameters(), lr=learning_rate, betas=[0.5, 0.999])
d_optimizer = optim.Adam(dnet.parameters(), lr=learning_rate, betas=[0.5, 0.999])

transform = transforms.Compose([transforms.Resize(32),transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5,))])
dataset = torchvision.datasets.MNIST(root='data/', train=True, transform=transform, download=True)
dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True, drop_last=True)

fixed_z = torch.randn([100, z_dim]).to(device)

for epoch in range(total_epochs):
    gnet = gnet.train()
    for i, data in enumerate(dataloader):
        real_images, _ = data
        real_images = real_images.to(device)
        z = torch.randn([batch_size, z_dim]).to(device)
        fake_images = gnet(z)

        real_loss = bce(dnet(real_images), ones)
        fake_loss = bce(dnet(fake_images.detach()), zeros)
        d_loss = real_loss + fake_loss

        d_optimizer.zero_grad()
        d_loss.backward()
        d_optimizer.step()

        g_loss = bce(dnet(fake_images), ones)
        g_optimizer.zero_grad()
        g_loss.backward()
        g_optimizer.step()
        print("Epoch:%d/%d  Batch:%d/%d -----D_loss:%f   G_loss:%f" % (
        epoch, total_epochs, i, len(dataloader), d_loss, g_loss))

    gnet = gnet.eval()
    fixed_fake_images = gnet(fixed_z)
    save_image(fixed_fake_images, 'DCgan_images/{}.png'.format(epoch), nrow=10, normalize=True)
